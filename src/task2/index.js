//  function for reverse string direction
function reverseString(str) {
  return str.split('').reverse().join('');
}

//  function tests if a string starts with a specified substring
function startWith(str, substr) {
  return str.indexOf(substr) === 0;
}

//  function tests if a string ends with a specified substring
function endWith(str, substr) {
  return str.lastIndexOf(substr) + substr.length === str.length;
}

//  function tests if string style is camelcase
function isCamelCase(str) {
  // check whitespaces and punctuation symbols
  var invalidCharacters = /[\s.!?,_]/;
  return !invalidCharacters.test(str);
}

//  function tests if string style is snakecase
function isSnakeCase(str) {
  // check symbol _
  var regularExpr = /\w+_\w+/;
  // regular expression to find uppercase word characters or whitespace characters.
  var invalidCharacters = /\s|[A-Z]/;
  return regularExpr.test(str) && !invalidCharacters.test(str);
}

//  function tests if value is falsy(0, null, '', false, null, NaN)
function isFalsy(x) {
  return !x;
}

//  function tests if value is not a number
function isNaN(x) {
  var value = x;
  return value !== x;
}

//  function tests if value is finite number
function isFinite(x) {
  return typeof x === 'number' && !isNaN(x)
      && x !== Number.POSITIVE_INFINITY
      && x !== Number.NEGATIVE_INFINITY;
}

if (window.globals && !window.globals.isTest) {
  reverseString();
  startWith();
  endWith();
  isCamelCase();
  isSnakeCase();
  isFalsy();
  isNaN();
  isFinite();
}
