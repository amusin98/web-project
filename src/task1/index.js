//  for calculating sine and cosine
const TERMS = 50;

//  calculating factorial of number
function factorial(x) {
  if (x < 0) {
    return NaN;
  }
  return x === 1 || x === 0 ? 1 : x * factorial(x - 1);
}

//  calculating module of number
function abs(x) {
  return x > 0 ? x : -x;
}

//  rounding number to nearest integer
function round(x) {
  return x > 0 ? parseInt(x + 0.5, 10) : parseInt(x - 0.4, 10);
}

//  raise number to the power
function sqrN(x, n) {
  var degree = n;
  //  if n if fractional number(rounding power to nearest integer not equal source power) -> NaN
  if (round(degree) !== degree) {
    return NaN;
  }
  var result = 1;
  while (degree !== 0) {
    if (degree > 0) {
      result *= x;
      degree--;
    } else {
      result /= x;
      degree++;
    }
  }
  return result;
}

//  function for round number to some decimal places
function roundTo(number, signCount) {
  var value = sqrN(10, signCount);
  return round(number * value) / value;
}

//  percent from number
function percentage(x, percent) {
  return roundTo(x / 100 * percent, 2);
}

//  calculate sine of x(taylor series)
function sinByTailroSeries(x) {
  var result = 0;
  for (var i = 0; i < TERMS; i++) {
    result += sqrN(-1, i) * sqrN(x, 2 * i + 1) / factorial(2 * i + 1);
  }
  return result;
}

//  calculate cosine of x(taylor series)
function cosByTailroSeries(x) {
  var result = 0;
  for (var i = 0; i < TERMS; i++) {
    result += sqrN(-1, i) * sqrN(x, 2 * i) / factorial(2 * i);
  }
  return result;
}

//  calculate sine of x
function sin(x) {
  return roundTo(sinByTailroSeries(x), 3);
}

//  calculate cosine of x
function cos(x) {
  return roundTo(cosByTailroSeries(x), 3);
}

//  calculate tangent of x
function tg(x) {
  return roundTo(sinByTailroSeries(x) / cosByTailroSeries(x), 3);
}


if (window.globals && !window.globals.isTest) {
  percentage();
  factorial();
  round();
  sqrN();
  abs();
  sin();
  cos();
  tg();
}
