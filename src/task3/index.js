// Returns array without n last elements
function initial(array, n) {
  var count = n === undefined ? 1 : n;
  count = count > array.length ? array.length : count;
  return array.slice(0, array.length - count);
}

// Returns array without falsy values
function compact(array) {
  return array.filter(function (item) {
    return item;
  });
}

// Returns array of unique elements
function uniq(arr) {
  return arr.filter(function (item, index, array) {
    return array.indexOf(item) === index;
  });
}

// Returns array of unique elements of all arrays
function union() {
  var arrays = [];
  Array.prototype.forEach.call(arguments, function (item) {
    arrays = arrays.concat(item);
  });
  return uniq(arrays);
}

// Returns true, if argument is array
function isArray(arr) {
  return arr instanceof Array;
}

// Returns difference of arrays
function difference(arr) {
  var array = arr || [];
  var arrays = [];
  for (var i = 1; i < arguments.length; i++) {
    arrays = arrays.concat(arguments[i]);
  }
  return array.filter(function (item) {
    return arrays.indexOf(item) === -1;
  });
}

// Returns array without n first elements
function rest(arr, n) {
  var count = n === undefined ? 1 : n;
  count = count < 0 ? 0 : count;
  return arr.slice(count, arr.length);
}

// Returns array of numbers from start(include) to stop(exclude) with some step
function range() {
  var start = 0;
  var stop = arguments[0];
  var step = arguments[2] || 1;
  if (arguments.length >= 2 && arguments[1] !== null) {
    start = arguments[0];
    stop = arguments[1];
  }
  if ((start > stop && step >= 1) || (start < stop) && step < 1) {
    return [];
  }
  var size = +((stop - start) / step + 0.3).toFixed(0);
  var array = new Array(size);
  for (var index = 0; index < size; index++) {
    array[index] = start;
    start += step;
  }
  return array;
}


if (window.globals && !window.globals.isTest) {
  initial();
  compact();
  union();
  isArray();
  difference();
  uniq();
  rest();
  range();
}
