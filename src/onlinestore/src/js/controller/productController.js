export default class ProductController {
  constructor(view, service) {
    this.view = view;
    this.service = service;
    this.init();
  }

  init() {
    console.log('controller');
    this.view.getProductsEvent.attach(this.getAllProducts.bind(this));
    this.view.getProductEvent.attach(this.getProduct.bind(this));
    this.view.addProductEvent.attach(this.addProduct.bind(this));
    this.view.updateProductEvent.attach(this.updateProduct.bind(this));
    this.view.deleteProductEvent.attach(this.deleteProduct.bind(this));
  }

  getProduct(sender, name) {
    this.service.getAll()
      .then(
        response => {
          let viewModel = JSON.parse(response);
          this.view.viewProducts(viewModel.products.filter(product => product.name === name), viewModel.userDTO);
        },
        error => console.log(error)
      )
  }

  updateProduct(sender, data) {
    this.service.update(data.id, data.product)
      .then(
        () => this.getAllProducts(),
        error => console.log(error)
      )
  }

  deleteProduct(sender, id) {
    this.service.delete(id)
      .then(
        () => this.getAllProducts(),
        error => console.log(error)
      )
  }

  getAllProducts() {
    this.service.getAll()
      .then(response => {
          let viewModel = JSON.parse(response);
          this.view.viewProducts(viewModel.products, viewModel.userDTO);
        },
        error => console.log(error));
  }

  addProduct(sender, product) {
    this.service.create(product)
      .then(
        () => this.getAllProducts(),
        error => console.log(error)
      )
  }


}