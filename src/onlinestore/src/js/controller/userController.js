export default class UserController {
  constructor(view, service) {
    this.view = view;
    this.service = service;
    this.init();
  }

  setProductView(productView) {
    this.productView = productView;
  }

  init() {
    this.view.signupEvent.attach(this.signup.bind(this));
    this.view.loginEvent.attach(this.login.bind(this));
    this.view.logoutEvent.attach(this.logout.bind(this));
    this.view.getUsersEvent.attach(this.getUsers.bind(this));
    this.view.getUserEvent.attach(this.getUser.bind(this));
    this.view.deleteUserEvent.attach(this.deleteUser.bind(this));
    this.view.updateUserEvent.attach(this.updateUser.bind(this));
  }

  getUser(sender, email) {
    this.service.getAll()
      .then(
        response => {
          let viewModel = JSON.parse(response);
          this.view.displayUsers(viewModel.users.filter(user => user.email === email), viewModel.userDTO);
        },
        error => console.log(error)
      )
  }

  updateUser(sender, data) {
    this.service.update(data.id, data.user)
      .then(
        () => this.getUsers(),
        error => console.log(error)
      )
  }

  deleteUser(sender, id) {
    this.service.delete(id)
      .then(
        () => this.getUsers(),
        error => {
          console.log(error);
        }
      )
  }

  getUsers() {
    this.service.getAll()
      .then(
        response => {
          let viewModel = JSON.parse(response);
          this.view.displayUsers(viewModel.users, viewModel.userDTO);
        },
        error => console.log(error)
      )
  }


  signup(sender, data) {
    console.log(sender);
    this.service.create(data.user)
      .then(() => {
          if (data.fromAdmin) {
            this.getUsers();
          } else {
            this.view.generateLoginInterface();
          }
        },
        error => console.log(error));

  }

  login(sender, data) {
    this.service.login(data)
      .then(response => {
          this.view.generateUserPage(JSON.parse(response));
          this.productView.addProductButton(JSON.parse(response))
        },
        error => console.log(error)
      );
  }

  logout() {
    this.service.logout();
    this.view.init();
    this.productView.init();
  }
}