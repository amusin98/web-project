import Event from "../event";
import Product from "../model/domain/product";

export default class ProductView {
  constructor(htmlService) {
    this.getProductsEvent = new Event(this);
    this.getProductEvent = new Event(this);
    this.addProductEvent = new Event(this);
    this.updateProductEvent = new Event(this);
    this.deleteProductEvent = new Event(this);
    this.htmlService = htmlService;
    this.init();
  }

  init() {
    let getProductsButton = document.getElementById('productsButton');
    getProductsButton.addEventListener('click', this.getProducts.bind(this));

    let getProductButton = document.getElementById('findProductButton');
    getProductButton.addEventListener('click', this.generateGetProductInterface.bind(this));
  }

  generateGetProductInterface() {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let form = this.htmlService.createForm(this.getProduct.bind(this));

    let nameLabel = this.htmlService.createLabel('Name');
    let nameInput = this.htmlService.createInput('text', 'name', true);

    let submit = this.htmlService.createButton('submit', 'Find');

    let reset = this.htmlService.createButton('reset', 'Cancel');

    form.appendChild(nameLabel);
    form.appendChild(nameInput);
    form.appendChild(submit);
    form.appendChild(reset);

    root.appendChild(form);
  }

  getProduct() {
    this.getProductEvent.notify(document.getElementById('name').value);
  }

  getProducts() {
    this.getProductsEvent.notify();
  }

  viewProducts(products, currentUser) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let table = document.createElement('table');

    let header = document.createElement('tr');

    let headerColumn1 = document.createElement('th');
    headerColumn1.innerHTML = 'Name';

    let headerColumn2 = document.createElement('th');
    headerColumn2.innerHTML = 'Description';

    header.appendChild(headerColumn1);
    header.appendChild(headerColumn2);
    table.appendChild(header);

    products.forEach(product => {
      let row = document.createElement('tr');

      let nameColumn = document.createElement('td');
      nameColumn.innerHTML = product.name;

      let descriptionColumn = document.createElement('td');
      descriptionColumn.innerHTML = product.description;

      row.appendChild(nameColumn);
      row.appendChild(descriptionColumn);


      if (currentUser !== null && currentUser.userId === product.ownerId) {
        let deleteColumn = document.createElement('td');
        let deleteButton = this.htmlService.createButton('button', 'Delete', this.deleteProduct.bind(this, product.productId));
        deleteColumn.appendChild(deleteButton);

        let updateColumn = document.createElement('td');
        let updateButton = this.htmlService.createButton('button', 'Update', this.generateUpdateProductInterface.bind(this, product));
        updateColumn.appendChild(updateButton);

        row.appendChild(deleteColumn);
        row.appendChild(updateColumn);
      }
      table.appendChild(row);
    });
    root.appendChild(table);
  }

  generateUpdateProductInterface(product) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let nameLabel = this.htmlService.createLabel('Name');
    let nameInput = this.htmlService.createInput('text', 'name', true);

    let descriptionLabel = this.htmlService.createLabel('Description');
    let descriptionInput = this.htmlService.createInput('text', 'description', true);

    let submit = this.htmlService.createButton('submit', 'Save');

    let reset = this.htmlService.createButton('reset', 'Reset');

    let form = this.htmlService.createForm(this.updateProduct.bind(this, product));

    form.appendChild(nameLabel);
    form.appendChild(nameInput);
    form.innerHTML += '</br>';

    form.appendChild(descriptionLabel);
    form.appendChild(descriptionInput);
    form.innerHTML += '</br>';

    form.appendChild(submit);
    form.appendChild(reset);

    root.appendChild(form);
  }

  updateProduct(product) {
    let name = document.getElementById('name').value;
    let description = document.getElementById('description').value;
    let updateTime = new Date();
    this.updateProductEvent.notify({
      id: product.productId,
      product: new Product(name, description, product.timeOfCreating, updateTime)
    });
  }

  deleteProduct(id) {
    this.deleteProductEvent.notify(id);
  }


  addProductButton(user) {
    let root = document.getElementById('root');
    if (user.isAdmin) {
      let createProductButton = this.htmlService.createButton('button', 'Add product', this.generateAddProductInterface.bind(this));
      document.body.insertBefore(createProductButton, root);
    }
  }

  generateAddProductInterface() {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let nameLabel = this.htmlService.createLabel('Name');
    let nameInput = this.htmlService.createInput('text', 'name', true);

    let descriptionLabel = this.htmlService.createLabel('Description');
    let descriptionInput = this.htmlService.createInput('text', 'description', true);

    let submitButton = this.htmlService.createButton('submit', 'Save');

    let resetButton = this.htmlService.createButton('reset', 'Reset');

    let form = this.htmlService.createForm(this.addProduct.bind(this));

    form.appendChild(nameLabel);
    form.appendChild(nameInput);
    form.innerHTML += '</br>';

    form.appendChild(descriptionLabel);
    form.appendChild(descriptionInput);
    form.innerHTML += '</br>';

    form.appendChild(submitButton);
    form.appendChild(resetButton);

    root.appendChild(form);
  }

  addProduct() {
    let date = new Date();
    this.addProductEvent.notify(new Product(
      document.getElementById('name').value,
      document.getElementById('description').value,
      date,
      date
    ));
  }
}