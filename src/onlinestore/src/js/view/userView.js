import User from "../model/domain/user";
import Event from "./../event";

export default class UserView {
  constructor(htmlService) {
    this.loginEvent = new Event(this);
    this.signupEvent = new Event(this);
    this.logoutEvent = new Event(this);
    this.updateUserEvent = new Event(this);
    this.deleteUserEvent = new Event(this);
    this.getUsersEvent = new Event(this);
    this.getUserEvent = new Event(this);
    this.htmlService = htmlService;
    this.init();
  }

  init() {
    document.body.innerHTML = '';
    let root = document.createElement('div');
    root.id = 'root';
    document.body.appendChild(root);

    let loginButton = this.htmlService.createButton('button', 'Login', this.generateLoginInterface.bind(this));
    loginButton.id = 'loginButton';

    let signUpButton = this.htmlService.createButton('button', 'Sign up', this.generateSignupInterface.bind(this, 0));
    signUpButton.id = 'signupButton';

    let productsButton = this.htmlService.createButton('button', 'Products');
    productsButton.id = 'productsButton';

    let findProductButton = this.htmlService.createButton('button', 'Find product');
    findProductButton.id = 'findProductButton';

    document.body.insertBefore(loginButton, root);
    document.body.insertBefore(signUpButton, root);
    document.body.insertBefore(productsButton, root);
    document.body.insertBefore(findProductButton, root);
  }

  generateSignupInterface(fromAdmin) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let nameLabel = this.htmlService.createLabel('Name');
    let nameInput = this.htmlService.createInput('text', 'name', true);

    let surnameLabel = this.htmlService.createLabel('Surname');
    let surnameInput = this.htmlService.createInput('text', 'surname', true);

    let patronymLabel = this.htmlService.createLabel('Patronym');
    let patronymInput = this.htmlService.createInput('text', 'patronym', true);

    let emailLabel = this.htmlService.createLabel('Email');
    let emailInput = this.htmlService.createInput('email', 'email', true);

    let passwordLabel = this.htmlService.createLabel('Password');
    let passwordInput = this.htmlService.createInput('password', 'password', true);

    let submitButton = this.htmlService.createButton('submit', 'Accept');

    let resetButton = this.htmlService.createButton('reset', 'Cancel');

    let form = this.htmlService.createForm(this.signup.bind(this, fromAdmin));

    form.appendChild(nameLabel);
    form.appendChild(nameInput);
    form.innerHTML += '</br>';
    form.appendChild(surnameLabel);
    form.appendChild(surnameInput);
    form.innerHTML += '</br>';
    form.appendChild(patronymLabel);
    form.appendChild(patronymInput);
    form.innerHTML += '</br>';
    form.appendChild(emailLabel);
    form.appendChild(emailInput);
    form.innerHTML += '</br>';
    form.appendChild(passwordLabel);
    form.appendChild(passwordInput);
    form.innerHTML += '</br>';
    form.appendChild(submitButton);
    form.appendChild(resetButton);

    root.appendChild(form);
  }

  signup(admin) {
    console.log(this);
    let name = document.getElementById('name').value;
    let surname = document.getElementById('surname').value;
    let patronym = document.getElementById('patronym').value;
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    this.signupEvent.notify({
      user: new User(name, surname, patronym, email, password),
      fromAdmin: admin
    });
  }

  generateLoginInterface() {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let emailLabel = this.htmlService.createLabel('Email');
    let emailInput = this.htmlService.createInput('email', 'email', true);

    let passwordLabel = this.htmlService.createLabel('Password');
    let passwordInput = this.htmlService.createInput('password', 'password', true);

    let submitButton = this.htmlService.createButton('submit', 'Accept');

    let resetButton = this.htmlService.createButton('reset', 'Cancel');

    let form = this.htmlService.createForm(this.login.bind(this));

    form.appendChild(emailLabel);
    form.appendChild(emailInput);
    form.innerHTML += '</br>';
    form.appendChild(passwordLabel);
    form.appendChild(passwordInput);
    form.innerHTML += '</br>';
    form.appendChild(submitButton);
    form.appendChild(resetButton);

    root.appendChild(form);
  }

  login() {
    let email = document.getElementById('email').value;
    let password = document.getElementById('password').value;
    this.loginEvent.notify({
      email: email,
      password: password
    })
  }

  generateUserPage(user) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let loginButton = document.getElementById('loginButton');
    let signupButton = document.getElementById('signupButton');
    loginButton.parentNode.removeChild(loginButton);
    signupButton.parentNode.removeChild(signupButton);

    let logout = this.htmlService.createButton('button', 'Logout', this.logout.bind(this));
    document.body.insertBefore(logout, root);

    let welcome = document.createElement('span');
    welcome.innerHTML = `Hello ${user.name}`;
    document.body.insertBefore(welcome, root);

    let profileButton = this.htmlService.createButton('button', 'Profile', this.getProfile.bind(this, user));
    document.body.insertBefore(profileButton, root);

    if (user.isAdmin) {
      let getUsersButton = this.htmlService.createButton('button', 'Users', this.getUsers.bind(this));

      let getUserButton = this.htmlService.createButton('button', 'Find user', this.generateGetUserInterface.bind(this));


      let createUserButton = this.htmlService.createButton('button', 'Create user', this.generateSignupInterface.bind(this, 1));
      document.body.insertBefore(getUsersButton, root);
      document.body.insertBefore(getUserButton, root);
      document.body.insertBefore(createUserButton, root);
    }
  }

  logout() {
    this.logoutEvent.notify();
  }

  getProfile(user) {
    let root = document.getElementById('root');
    root.innerHTML = '';
    root.innerHTML += `Name: ${user.name}`;
    root.innerHTML += '</br>';
    root.innerHTML += `Surname: ${user.surname}`;
    root.innerHTML += '</br>';
    root.innerHTML += `Patronymic: ${user.patronymic}`;
    root.innerHTML += '</br>';
    root.innerHTML += `Email: ${user.email}`;
  }

  getUsers() {
    this.getUsersEvent.notify();
  }


  generateGetUserInterface() {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let form = this.htmlService.createForm(this.getUser.bind(this));

    let emailLable = this.htmlService.createLabel('Email');
    let emailInput = this.htmlService.createInput('text', 'email', true);

    let submit = this.htmlService.createButton('submit', 'Find');

    let reset = this.htmlService.createButton('reset', 'Cancel');

    form.appendChild(emailLable);
    form.appendChild(emailInput);
    form.appendChild(submit);
    form.appendChild(reset);

    root.appendChild(form);
  }

  getUser() {
    this.getUserEvent.notify(document.getElementById('email').value);
  }


  displayUsers(users, currentUser) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let table = document.createElement('table');

    let header = document.createElement('tr');

    let headerColumn1 = document.createElement('th');
    headerColumn1.innerHTML = 'Name';

    let headerColumn2 = document.createElement('th');
    headerColumn2.innerHTML = 'Surname';

    let headerColumn3 = document.createElement('th');
    headerColumn3.innerHTML = 'Patronym';

    let headerColumn4 = document.createElement('th');
    headerColumn4.innerHTML = 'Email';

    let headerColumn5 = document.createElement('th');
    headerColumn5.innerHTML = 'Password';

    header.appendChild(headerColumn1);
    header.appendChild(headerColumn2);
    header.appendChild(headerColumn3);
    header.appendChild(headerColumn4);
    header.appendChild(headerColumn5);
    table.appendChild(header);

    users.forEach(user => {
      let row = document.createElement('tr');

      let nameColumn = document.createElement('td');
      nameColumn.innerHTML = user.name;

      let surnameColumn = document.createElement('td');
      surnameColumn.innerHTML = user.surname;

      let patronymColumn = document.createElement('td');
      patronymColumn.innerHTML = user.patronymic;

      let emailColumn = document.createElement('td');
      emailColumn.innerHTML = user.email;

      let passwordColumn = document.createElement('td');
      passwordColumn.innerHTML = user.password;


      row.appendChild(nameColumn);
      row.appendChild(surnameColumn);
      row.appendChild(patronymColumn);
      row.appendChild(emailColumn);
      row.appendChild(passwordColumn);

      if (currentUser.admin) {
        let deleteColumn = document.createElement('td');
        let deleteButton = this.htmlService.createButton('button', 'Delete', this.deleteUser.bind(this, user.userId));
        deleteColumn.appendChild(deleteButton);

        let updateColumn = document.createElement('td');
        let updateButton = this.htmlService.createButton('button', 'Update', this.generateUpdateUserInterface.bind(this, user));
        updateColumn.appendChild(updateButton);

        row.appendChild(deleteColumn);
        row.appendChild(updateColumn);
      }
      table.appendChild(row);
    });
    root.appendChild(table);
  }


  deleteUser(id) {
    this.deleteUserEvent.notify(id);
  }

  generateUpdateUserInterface(user) {
    let root = document.getElementById('root');
    root.innerHTML = '';

    let nameLabel = this.htmlService.createLabel('Name');
    let nameInput = this.htmlService.createInput('text', 'name', true);

    let surnameLabel = this.htmlService.createLabel('Surname');
    let surnameInput = this.htmlService.createInput('text', 'surname', true);

    let patronymLabel = this.htmlService.createLabel('Patronym');
    let patronymInput = this.htmlService.createInput('text', 'patronym', true);

    let emailLabel = this.htmlService.createLabel('Email');
    let emailInput = this.htmlService.createInput('email', 'email', true);

    let passwordLabel = this.htmlService.createLabel('Password');
    let passwordInput = this.htmlService.createInput('password', 'password', true);


    let submit = this.htmlService.createButton('submit', 'Save');

    let reset = this.htmlService.createButton('reset', 'Reset');

    let form = this.htmlService.createForm(this.updateUser.bind(this, user));

    form.appendChild(nameLabel);
    form.appendChild(nameInput);
    form.innerHTML += '</br>';

    form.appendChild(surnameLabel);
    form.appendChild(surnameInput);
    form.innerHTML += '</br>';

    form.appendChild(patronymLabel);
    form.appendChild(patronymInput);
    form.innerHTML += '</br>';

    form.appendChild(emailLabel);
    form.appendChild(emailInput);
    form.innerHTML += '</br>';

    form.appendChild(passwordLabel);
    form.appendChild(passwordInput);
    form.innerHTML += '</br>';

    form.appendChild(submit);
    form.appendChild(reset);

    root.appendChild(form);
  }

  updateUser(user) {
    let updatedUser = new User(
      document.getElementById('name').value,
      document.getElementById('surname').value,
      document.getElementById('patronym').value,
      document.getElementById('email').value,
      document.getElementById('password').value,
    );
    this.updateUserEvent.notify({
      id: user.userId,
      user: updatedUser
    });
  }

}