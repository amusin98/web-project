import ProductService from "./model/service/productService";
import UserService from "./model/service/userService";
import UserView from "./view/userView";
import ProductView from "./view/productsView";
import ProductController from "./controller/productController";
import UserController from "./controller/userController";
import HtmlElementService from "./model/service/htmlElementService";


let htmlService = new HtmlElementService();
let userController = new UserController(new UserView(htmlService), new UserService());
let productView = new ProductView(htmlService);
let productController = new ProductController(productView, new ProductService());
userController.setProductView(productView);