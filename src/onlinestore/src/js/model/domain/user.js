export default class User {
  constructor(name, surname, patronymic, email, password) {
    this.name = name;
    this.surname = surname;
    this.patronymic = patronymic;
    this.email = email;
    this.password = password;
  }
}

