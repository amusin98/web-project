export default class Product {
  constructor(name, description, timeOfCreating, timeOfEditing) {
    this.name = name;
    this.description = description;
    this.timeOfCreating = timeOfCreating;
    this.timeOfEditing = timeOfEditing;
  }
}
