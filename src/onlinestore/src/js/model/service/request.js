export default class Request {
  get(url) {
    return this.send(url, 'GET');
  }

  post(url, data) {
    return this.send(url, 'POST', data)
  }

  put(url, data) {
    return this.send(url, 'PUT', data);
  }

  delete(url) {
    return this.send(url, 'DELETE');
  }

  send(url, method, data) {
    return new Promise(function (resolve, reject) {
      let xhr = new XMLHttpRequest();
      xhr.open(method, url, true);
      xhr.withCredentials = true;
      xhr.setRequestHeader('Content-type', 'application/json');
      xhr.onload = function () {
        if (this.status < 400) {
          resolve(this.response);
        } else {
          let error = new Error(this.statusText);
          error.code = this.status;
          reject(error);
        }
      };
      data === undefined ? xhr.send() : xhr.send(JSON.stringify(data));
    })
  }
}