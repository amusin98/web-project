import Request from "./request"

export default class UserService {
  constructor() {
    this.server = 'http://localhost:8080/users';
  }


  getAll() {
    let request = new Request();
    return request.get(this.server);
  }

  getById(id) {
    let request = new Request();
    return request.get(this.server + `/${id}`);
  }

  create(data) {
    let request = new Request();
    return request.post(this.server + `/sign-up`, data);
  }

  update(id, data) {
    let request = new Request();
    return request.put(this.server + `/${id}`, data);
  }

  delete(id) {
    let request = new Request();
    return request.delete(this.server + `/${id}`);
  }

  login(data) {
    let request = new Request();
    return request.post('http://localhost:8080/login', data)
  }

  logout() {
    let request = new Request();
    return request.get(this.server + '/logout');
  }
}