import Request from "./request"


export default class ProductService {
  constructor() {
    this.server = 'http://localhost:8080/products';
  }


  getAll() {
    let request = new Request();
    return request.get(this.server);
  }

  getById(id) {
    let request = new Request();
    return request.get(this.server + `/${id}`);
  }

  create(data) {
    let request = new Request();
    return request.post(this.server, data);
  }

  update(id, data) {
    let request = new Request();
    return request.put(this.server + `/${id}`, data);
  }

  delete(id) {
    let request = new Request();
    return request.delete(this.server + `/${id}`);
  }
}