export default class HtmlElementService {

  createButton(type, innerHTML, click) {
    let button = document.createElement('button');
    button.type = type;
    button.innerHTML = innerHTML;
    button.addEventListener('click', click);
    if (type === 'submit' || type === 'reset') {
      button.className += ' formButton';
    }
    return button;
  }

  createLabel(labelText) {
    let label = document.createElement('label');
    label.innerHTML = labelText;
    return label;
  }

  createInput(type, id, required) {
    let input = document.createElement('input');
    input.type = type;
    input.id = id;
    input.required = required;
    return input;
  }

  createForm(submitEvent) {
    let form = document.createElement('form');
    form.addEventListener('submit', submitEvent);
    form.addEventListener('submit', (e) => e.preventDefault());
    return form;
  }
}