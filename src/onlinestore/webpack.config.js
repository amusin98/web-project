module.exports = {
  entry: './src/js/main',

  output: {
    filename: 'build.js'
  },

  module: {
    rules: [
      {
        test: /\.js$/,
        loader: "babel-loader"
      }
    ]
  },
};